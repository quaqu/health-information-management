// Package name
package project.structures;

//  Importing java scanner and file  library for easy program execution .
     
    import java.io.*;
    import java.util.*;


    import java.nio.file.Files;
    import java.nio.file.Path;
    import java.nio.file.Paths;
    import java.nio.file.StandardOpenOption;

/**
 * @authors
 * @version 3
 * 
 * Definition of a class, patient.
 */
public class Patient {
    // Instantiating  instant variable for  class attributes.
    public static ArrayList <Patient> patientList = new ArrayList<>();
    String firstName, lastName, middleName, dateOfBirth, placeOfBirth, homeAddress,
        phoneNumber, town, email,nhisNumber , fullname;
    int age;
    int ID;
    genderType gender;
    bloodGroupType bloodGroup;
    titleNames title;
    Date date;
    Date lastEdited;

    // Instant variable for non-static details.
    ArrayList<Double>  weight = new ArrayList<>();
    ArrayList<Double>  height = new ArrayList<>();
    ArrayList<Double>  temperature = new ArrayList<>();
    ArrayList<Double>  bloodPressure = new ArrayList<>();
    ArrayList<String>  diseases = new ArrayList<>();
    ArrayList<String>  medication = new ArrayList<>();
    /**
     *  Enum for specifying the gender types for input restrictions.
     */
    private enum genderType{
        female, male;

        // Gender method to retrieving  the gender you an integer.
        public static  genderType getGenderType(int  ordinal){
            genderType genderValue = null;
            for(genderType value : genderType.values()){
                if(value.ordinal() == ordinal){
                    genderValue= value;
                    break;
                }else genderValue = null;
            }
            return  genderValue;
        }
    }

    /**
     *  Enum for restricting  the user input of  the blood group .
     */
    private enum bloodGroupType{
        A_positive, A_negative, B_positive, B_negative,
            O_positive, O_negative, AB_positive, AB_negative;

        //  Method to  retrieve the blood group using an integer.
        public static  bloodGroupType getBloodType(int  ordinal){
            bloodGroupType bloodValue = null;
            for(bloodGroupType value : bloodGroupType.values()){
                if(value.ordinal() == ordinal){
                    bloodValue = value;
                    break;
                }else bloodValue = null;
            }
            return  bloodValue ;
        }

    }

    /**
     * Enum for restricting user  input on the title names.
     */
    private enum titleNames{
        Mrs, Miss,Ms, Mr;

        // Method to retrieve the title names using integers.
        public static  titleNames getTitleNames(int  ordinal){
            titleNames titleValue = null;
            for(titleNames value : titleNames.values()){
                if(value.ordinal() == ordinal){
                    titleValue = value;
                    break;
                }else titleValue = null;
            }
            return  titleValue ;
        }
    }

    /**
     *  Default patient constructor .
     */
    public Patient(){ 
        Random rand = new Random();
        ID = rand.nextInt(10000000)+100000;
        date = new Date();
    }

    /**
     *
     * @param title
     * @param firstName
     * @param middleName
     * @param lastName
     * @param gender
     * @param dateOfBirth
     * @param age
     * @param placeOfBirth
     * @param homeAddress
     * @param town
     * @param bloodGroup
     * @param nhisNumber
     * @param phoneNumber
     * @param email
     *
     * Overloaded Patient constructor.
     */
    public Patient(titleNames title, String firstName, String middleName, String lastName, genderType gender,
                    String dateOfBirth, int age, String placeOfBirth, String homeAddress, String town, 
                    bloodGroupType bloodGroup,String nhisNumber, String phoneNumber, String email ){ 
                        this.title = title;
                        this.firstName = firstName;
                        this.middleName = middleName;
                        this.lastName = lastName;
                        this.gender = gender;
                        this.dateOfBirth = dateOfBirth;
                        this.age = age;
                        this.placeOfBirth = placeOfBirth;
                        this.homeAddress = homeAddress;
                        this.town = town;
                        this.bloodGroup = bloodGroup;
                        this.nhisNumber = nhisNumber;
                        this.phoneNumber = phoneNumber;
                        this. email = email;
                        Random rand = new Random();
                        ID = rand.nextInt(10000000)+100000;
                        date = new Date();
                        lastEdited = new Date();
                    }

    /**
     * Setters or mutator  methods for amending a patient.
     */

    // Mutator method for first name.
    public void setFirstName(String name){
        if(name != null){ 
            this.firstName = name;
            lastEdited = new Date();
        }
    }

    // Mutator method for last name.
    public void setLastName(String name){
        if (name != null) {
            this.lastName = name;
            lastEdited = new Date();
        }
    }

    // Mutator method for middle name .
    public void setMiddleName(String name){
        if(name != null){ 
            this.middleName = name;
            lastEdited = new Date();
        }
    }

    // Mutator method for date of birth.
    public  void setDateOfBirth(String date){
            if(date != null){
                this.dateOfBirth = date;
                lastEdited = new Date();
            }
    }

    // Mutator method for  home address.
    public void setHomeAddress(String address){
            if (address != null){
                this.homeAddress = address;
                lastEdited = new Date();
            }
    }

    // Mutator method for phone number.
    public void setPhoneNumber(String phone){
            if (phone != null) {
                this.phoneNumber = phone;
                lastEdited = new Date();
            }
    }

    // Mutator method for town.
    public void setTown(String town){
          if (town != null){ 
              this.town = town;
              lastEdited = new Date();
            }
    }

    // Mutator method for email.
    public void setEmail(String email){
           if (email != null) {
               this.email = email;
               lastEdited = new Date();
            }
    }

    // Mutator method for age.
    public void setAge(int age ) {
        if (age != 0 ){
             this.age = age; 
             lastEdited = new Date();}
    }

    // Mutator method for gender.
    public void setGender(genderType gender){
            if (gender != null) {
                this.gender = gender;
                lastEdited = new Date();
            }
    }

    // Mutator method for blood group.
    public void setBloodGroup(bloodGroupType bloodType){
            if (bloodType != null){
                this.bloodGroup = bloodType;
                lastEdited = new Date();
            }
    }

    // Mutator method for title.
    public  void setTitle(titleNames title){
            if (title != null) {
                this.title= title;
                lastEdited = new Date();
            }
    }

    // Mutator method for NHIS number.
    public void setNhisNumber(String nhis){
            if (nhis !=null) {
                this.nhisNumber = nhis;
                lastEdited = new Date();
            }
    }

    // Mutator method for place of birth.
    public void setPlaceOfBirth(String place){
            if (place != null){
                this.placeOfBirth = place;
                lastEdited = new Date();
            }
    }

    // Mutator method for height list
    public void setHeight(Double height){
        if (height != null){
            this.height.add(height);
            lastEdited = new Date();
        }   
    }

    // Mutator method for weight list
    public void setWeight(Double weight){
        if (weight != null){
            this.weight.add(weight);
            lastEdited = new Date();
        }
    }

    // Mutator method for temperature list
    public void setTemperature(Double temperature){
        if (temperature != null){
            this.temperature.add(temperature);
            lastEdited = new Date();
        }   
    }

    // Mutator method for blood pressure list
    public void setBloodPressure(Double bloodPressure){
        if (bloodPressure != null){
            this.bloodPressure.add(bloodPressure);
            lastEdited = new Date();
        }
    }

    // Mutator method for disease list
    public void setDiseases(String disease){
        if (disease != null){
            this.diseases.add(disease);
            lastEdited = new Date();
        }
    }

    // Mutator method for medication list 
    public void setMedication(String medicine){
        if (medicine != null){
            this.medication.add(medicine);
            lastEdited = new Date();
        }
    }
    /**
     *
     * @return
     *
     *  Getters or  Accessor method
     */

    // Accessor method  for gender.
      public genderType getGender() {return gender; }

    // Accessor method  for first name.
      public String getFirstName() { return firstName; }

    // Accessor method  for last name.
      public String getLastName() { return lastName; }

    // Accessor method  for middle name.
      public String getMiddleName() { return middleName; }

    // Accessor method  for phone number .
      public String getPhoneNumber() { return phoneNumber; }

    // Accessor method  for NHIS number.
      public String getNhisNumber() { return nhisNumber; }

    // Accessor method  for  age.
      public int getAge() { return age; }

    // Accessor method for date of birth.
      public String getDateOfBirth() { return dateOfBirth; }

    // Accessor method  for email.
      public String getEmail() { return email; }

    // Accessor method  for home address.
      public String getHomeAddress() { return homeAddress; }

    // Accessor method  for  town.
      public String getTown() { return town; }

    // Accessor method  for title.
      public titleNames getTitle() { return title; }

    // Accessor method  for blood group.
	public bloodGroupType getBloodGroup() { return bloodGroup; }

    // Accessor method for place of birth.
    public String getPlaceOfBirth() {return placeOfBirth; }

    // Accessor method for full name.
    public String getFullname(){
          if (middleName == null){
              return this. fullname = getFirstName() + " "+getLastName();
          }else{
              return  this.fullname = getFirstName() + " "+getMiddleName()+" "+getLastName();
          }
    }

    public  int getID(){
            return this.ID;
    }

    // Accessor method date.
    public Date getDate(){ return this.date; }

    // Accessor method  last edited.
    public Date getLastEdited(){ return this.lastEdited; }
    
    // Accessor method for the weight list
    public ArrayList<Double> getWeight(){
        return this.weight;
    }

    //Accessor method for the height list
    public ArrayList<Double> getHeight(){
        return this.height;
    }
    
    // Accessor method for the temperature list
    public ArrayList<Double> getTemperature(){
        return this.temperature;
    }

    // Accessor method for the blood pressure list
    public ArrayList<Double> getBloodPressure(){
        return this.bloodPressure;
    }

    // Accessor method for the disease list
    public ArrayList<String> getDisease(){
        return this.diseases;
    }

    // Accessor method for medication list.
    public ArrayList<String> getMedication(){
        return this.medication;
    }

    /**
     *
     * @param patient
     *
     *  Mutator method , setPatientInfo to editing the  patient information using the terminal.
     */
    public void setPatientInfo(Patient patient){

        // Instantiating variables for mutating the patient class.
        int titleNumber=-1;
        String phoneNum = null;
        String first_name=null;
        String last_name=null;
        String middle_name=null;
        int genNumber=-1;
        int bloodNumber= -1;
        int realAge = -1;
        String emailAdd=null;
        String NHISnumber= null;
        String residenceTown= null;
        String placeBirth= null;
        String dateBirth= null;

        // Starting the scanner class to allow input.
        Scanner input = new Scanner(System.in);
        Scanner userInput;
        // Instructions  to facilitate easy execution.
        System.out.println(String.format("%15s %10s %10s %10s %10s  %10s %10s  %10s %10s ", "Title :  ", "|", "Mrs", "|", "Miss", "|", "Ms", "|", "Mr"));
        System.out.println(String.format("%s", "-----------------------------------------------------------------------------------------------------------------"));
        System.out.println(String.format("%15s %10s %10s %10s %10s  %10s %10s  %10s %10s", "Input Number : ", "|", "0", "|", "1", "|", "2", "|", "3"));
        System.out.print("Enter title number : ");

        //  Checking and processing title input .
        while(input.hasNext()){
            userInput = new Scanner(input.next());
            if(userInput.hasNextInt()){
                titleNumber = userInput.nextInt();
                if(titleNumber < 0 || titleNumber > 3){
                    System.out.print("Enter the above integers: ");
                    continue;
                }
            }
            else{
                System.out.print("Enter the above integers: ");
                continue;
            }
            break;
        }

        //  Setting title using user input.
        titleNames titleName= titleNames.getTitleNames(titleNumber);
        patient.setTitle(titleName);
        System.out.println("Title: " + getTitle());

        // Preamble  for  clarity in instructions.
        System.out.println("Enter full name in the form; First_name Last_name Middle_name : ");

        // Checking and processing names input.
        while(input.hasNextLine()){
            userInput = new Scanner(input.nextLine());
            if(userInput.hasNextInt()){
                System.out.print("Enter proper name: ");
                continue;
            }else if(userInput.hasNext()){
               first_name = userInput.next();
            }else{
                continue;
            }

            if(userInput.hasNextInt()){
                System.out.print("Enter proper Last name: ");
                continue;
            }else if(userInput.hasNext()){
                last_name = userInput.next();
            }else{
                System.out.println("No last name, check again! ");
                continue;
            }

            if(userInput.hasNextInt()){
                System.out.print("Enter proper middle name: ");
                continue;
            }else if(userInput.hasNext()){
                middle_name = userInput.next();
            }
            break;
        }

        // Setting first name using user input .
        patient.setFirstName(first_name);
        System.out.println("First name: " + getFirstName());

        // Setting middle name using user input .
        patient.setMiddleName(middle_name);
        System.out.println("Middle name: " + getMiddleName());

        // Setting last name using user input .
        patient.setLastName(last_name);
        System.out.println("Last name: " + getLastName());

        // Preamble for clarity of instruction.
        System.out.println(String.format("%15s %10s %10s %10s %10s", "Gender :  ", "|", "Female", "|", "Male"));
        System.out.println(String.format("%s", "---------------------------------------------------------------"));
        System.out.println(String.format("%15s %10s %10s %10s %10s", "Input Number : ", "|", "0", "|", "1"));
        System.out.print("Enter Gender number: ");

        // Checking and processing gender input.
        while(input.hasNext()){
            userInput = new Scanner(input.next());
            if(userInput.hasNextInt()){
                genNumber = userInput.nextInt();
                if(genNumber < 0 || genNumber> 1){
                    System.out.print("Enter the above integers: ");
                    continue;
                }
            }
            else{
                System.out.print("Enter the above integers: ");
                continue;
            }
            break;
        }

        // Setting gender using user input.
        genderType gentype= genderType.getGenderType(genNumber);
        patient.setGender(gentype);
        System.out.println("Gender: " + getGender());

        // Preamble for clarity of instructions.
        System.out.println(String.format("%15s %10s %10s %10s %10s %10s %10s %10s  %10s","Blood Group : ", "|", "A_positive", "|", " A_negative","|"," B_positive" ,"|", " B_negative"));
        System.out.println(String.format("%s", "-------------------------------------------------------------------------------------------------------"));
        System.out.println(String.format("%15s %10s %10s %10s %10s %10s %10s %10s  %10s %10s", "Input Number", "|", "0", "|", "1","|"," 2" ,"|", " 3","|"));
        System.out.println();
        System.out.println(String.format("%15s %10s %10s %10s %10s %10s %10s %10s  %10s","Blood Group : ", "|", " O_positive","|", " O_negative", "|"," AB_positive","|"," AB_negative"));
        System.out.println(String.format("%s", "--------------------------------------------------------------------------------------------------------"));
        System.out.println(String.format("%15s %10s %10s %10s %10s %10s %10s %10s  %10s", "Input Number", "|", " 4","|", " 5", "|"," 6","|"," 7"));
        System.out.print("Enter blood group number, enter 'n/N' if blood group is unknown : ");

        //  Checking processing blood group input.
        while(input.hasNext()){
            userInput = new Scanner(input.next());
            if(userInput.hasNextInt()){
                bloodNumber= userInput.nextInt();
                if(bloodNumber < 0 || bloodNumber > 7){
                    System.out.print("Enter the above integers: ");
                    continue;
                }
            }else if(userInput.hasNext()){
                String exit = userInput.next();
                if(exit.equals("n") || exit.equals("N")) {
                    System.out.println("Blood group unknown! ");
                    break;
                }
                else{
                    System.out.print("Enter the above integers: ");
                    continue;
                }
            }else{
                System.out.print("Enter the above integers: ");
                continue;
            }
            break;
        }

        //  Setting blood group using user input.
        bloodGroupType bloodname= bloodGroupType.getBloodType(bloodNumber);
        patient.setBloodGroup(bloodname);
        System.out.println("Blood Group: " + getBloodGroup());

        // Preamble for clarity of instructions.
        System.out.print("Enter date of birth in the form; day month year : ");
        // Checking and processing date of birth and age input.
        while(input.hasNextLine()){
            int day,month,year;
            userInput = new Scanner(input.nextLine());
            if(userInput.hasNextInt()){
                day= userInput.nextInt();
                if(day <1 || day>31){
                    System.out.print("Check the day:");
                    continue;
                }
            }else{
                continue;
            }
            if(userInput.hasNextInt()){
                month = userInput.nextInt();
                if(month <1 || month>12){
                    System.out.print("Check the month:");
                    continue;
                }
            }else{
                continue;
            }
            if(userInput.hasNextInt()){
                year = userInput.nextInt();
            }else{
                System.out.print("Check the year: ");
                continue;
            }
            // Calculating age using  user input.
            realAge = Calendar.getInstance().get(Calendar.YEAR)-year;
            dateBirth = day + "/"+month+"/"+year;
            break;
        }

        //  Setting date of birth using  user input.
        patient.setDateOfBirth(dateBirth);

        // Setting age using  user input.
        patient.setAge(realAge);
        System.out.println("Age: " + getAge());

        //  Preamble for clarity of  instructions.
        System.out.print("Enter place of birth : ");

        //  Checking and processing place of birth input.
        while(input.hasNext()){
            userInput = new Scanner(input.next());
            if(userInput.hasNextInt()){
                System.out.print("Enter proper place: ");
                continue;
            }else if(userInput.hasNext()){
                placeBirth = userInput.next();
            }
            else{
                System.out.print("Enter place of birth: ");
                continue;
            }
            break;
        }

        // Setting the place of birth  using user input.
        patient.setPlaceOfBirth(placeBirth);
        System.out.println("Place of birth: " + getPlaceOfBirth());

        // Preamble for  clarity of instructions.
        System.out.print("Enter home address : ");
        String addresshome= null;

        // Checking processing the home address input
        while(input.hasNextLine()){
            userInput = new Scanner(input.nextLine());
            String addresshome1 = "";
            String addresshome2 = "";
            String addresshome3 = "";
            String addresshome4 = "";

            if(userInput.hasNextInt()){
                System.out.print("Enter proper home addresss: ");
                continue;
            }else if(userInput.hasNext()){
                addresshome1 = userInput.next();
                addresshome = addresshome1 ;
            }else{
                System.out.print("Enter home address: ");
                continue;
            }
            if(userInput.hasNext()){
                addresshome2 = userInput.next();
                addresshome = addresshome1 +" " + addresshome2;
            }
            if(userInput.hasNext()){
                addresshome3 = userInput.next();
                addresshome = addresshome1 + " "+addresshome2 +" "+ addresshome3;
            }
            if(userInput.hasNext()){
                addresshome4 = userInput.next();
                addresshome = addresshome1 +" "+ addresshome2 + " "+addresshome3 + " "+addresshome4;
            }
            break;
        }

        // Setting home address using input.
        patient.setHomeAddress(addresshome);
        System.out.println("Home address: " +  getHomeAddress());

        // Preamble for clarity of instructions.
        System.out.print("Enter the town of residence : ");
        // Checking and processing input for town of residence.
        while(input.hasNext()){
            userInput = new Scanner(input.next());
            if(userInput.hasNextInt()){
                System.out.print("Enter proper town: ");
                continue;
            }else if(userInput.hasNext()){
                residenceTown = userInput.next();
            }
            else{
                System.out.print("Enter the town of residence : ");
                continue;
            }
            break;
        }

        // Setting town of residence using input.
        patient.setTown(residenceTown);
        System.out.println("Town of residence: " + getTown());

        // Preamble for clarity of instructions.
        System.out.print("Enter NHIS number, enter 'n/N' if patient do not have  NHIS number: ");
        // Checking and processing input for NHIS number.
        while(input.hasNext()){
            userInput = new Scanner(input.next());
            if(userInput.hasNext()){
                 String exit= userInput.next();
                if(exit.equals("n") || exit.equals("N")){
                    System.out.println("No NHIS number!");
                    break; 
                }else {NHISnumber = exit;}
            }
            else{
                System.out.println("Enter NHIS number");
                continue;
            }
            break;
        }

        // Setting NHIS number using input.
        patient.setNhisNumber(NHISnumber);
        System.out.println("NHIS Number : " + getNhisNumber());

        // Preamble for  clarity of instructions.
        System.out.print("Enter phone number, enter 'n/N' if patient do not have phone number : ");
        // Checking and processing input for phone number.
        while(input.hasNext()){
            userInput = new Scanner(input.next());
            if(userInput.hasNext()){
                String exit = userInput.next();
                if(exit.equals("n") || exit.equals("N")){
                    System.out.println("No phone number! ");
                    break; 
                }else {phoneNum = exit;}
            }
            else{
                System.out.print("Enter phone number: ");
                continue;
            }
            break;
        }

        //  Setting phone number using input.
        patient.setPhoneNumber(phoneNum);
        System.out.println("Phone Number: " + getPhoneNumber());

        // Preamble for clarity of  instructions.
        System.out.print("Enter the E-mail address enter 'n/N' if patient do not have email address: ");
        // Checking and processing input for email.
        while(input.hasNext()){
            userInput = new Scanner(input.next());
            if(userInput.hasNext()){
                String exit = userInput.next();
                if(exit.equals("n") || exit.equals("N")){
                    System.out.println("No email address!");
                    break;
                }else {emailAdd = exit;}
            }
            else{
                System.out.println("Enter email address;");
                continue;
            }
            break;
        }

        // Setting email using input.
        patient.setEmail(emailAdd);
        System.out.println("Email Address : " + getEmail());

        // Generating random number for user ID.
        Random rand = new Random();
        ID = rand.nextInt(10000000)+100000;
        input.close();

        // Adding patient to the patient list.
        patientList.add(patient);
        
    }

    /**
     *  Records methods to set the measured data.
      * @param height
     * @param weight
     * @param temperature
     * @param bloodPressure
     */
    public void setRecords(Double height, Double weight, Double temperature, Double bloodPressure){
        setHeight(height);
        setWeight(weight);
        setTemperature(temperature);
        setBloodPressure(bloodPressure);
        lastEdited = new Date();

    }

    /**
     *  Records method to set the disease and the medicine prescription
     * @param medicine
     * @param disease
     */
    public void setPhamaRecords(String medicine, String disease ){
        setMedication(medicine);
        setDiseases(disease);
        lastEdited = new Date();
    }

    public  void search(String fullname){
        boolean  isFound = false;
        for (Patient  p: patientList){
            if (p.getFullname().equals(fullname)){
                isFound = true;
                System.out.println(p.getID());
            }
            if(isFound == false){
                System.out.println("Patient do not exist for the name");
            }
        }
    }

    /**
     *
     * @return
     *  To string method for returning patient information.
     */
    @Override
		public String toString() {
			return "Patient: " + "\n"+ "title:   " + title + "\n"+"firstName:   " + firstName +"\n"+ "lastName:   "
                    + lastName + "\n"+"middleName:   " + middleName + "\n"+"ID:   " + ID + "\n"+"dateOfBirth:    "
                    + dateOfBirth + "\n"+"age:   " + age + "\n"+"placeOfBirth:   " + placeOfBirth + "\n"+"gender:   "
                    + gender + "\n"+"homeAddress:   " + homeAddress + "\n"+"phoneNumber:   "
                    + phoneNumber + "\n"+"town:   " + town + "\n"+"email:   " + email + "\n"+"nhisNumber:   "
					+ nhisNumber + "\n"+"bloodGroup:   " + bloodGroup + "\n";
		}

    /**
     *  Print profile method for displays patient information in a tables for easy readability.
     */
        public void printProfile(){
            System.out.println("Profile: ");
            System.out.println(String.format("%s", "---------------------------------------------------------"));
            if(getMiddleName() == null){
                System.out.println(String.format("%15s %20s %15s %5s %5s %5s", "|", "Name", "|", getTitle(), getFirstName(),getLastName()));
            }else{
                System.out.println(String.format("%15s %20s %15s %5s %5s %5s %5s", "|", "Name", "|", getTitle(), getFirstName(),getMiddleName(),getLastName()));
            }
            System.out.println(String.format("%15s %20s %15s %15s", "|", "ID", "|", getID() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Gender", "|", getGender() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Date of birth", "|", getDateOfBirth() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Age", "|", getAge() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Place of birth", "|", getPlaceOfBirth() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Blood Group", "|", getBloodGroup() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Home Address", "|", getHomeAddress() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Town of Residence", "|", getTown() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "NHIS Number", "|", getNhisNumber() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Phone Number", "|", getPhoneNumber()));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Email Address", "|", getEmail() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Date Created", "|", getDate() ));
            System.out.println(String.format("%15s %20s %15s %15s", "|", "Last Edited ", "|", getLastEdited() ));

        }

    /**
     *
     * @param obj
     * @return
     *   Equals methods to compare object to patient class.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Patient other = (Patient) obj;
        if (age != other.age)
            return false;
        if (ID != other.ID)
            return false;
        if (bloodGroup != other.bloodGroup)
            return false;
        if (dateOfBirth == null) {
            if (other.dateOfBirth != null)
                return false;
        } else if (!dateOfBirth.equals(other.dateOfBirth))
            return false;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;
        if (gender != other.gender)
            return false;
        if (homeAddress == null) {
            if (other.homeAddress != null)
                return false;
        } else if (!homeAddress.equals(other.homeAddress))
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
        if (middleName == null) {
            if (other.middleName != null)
                return false;
        } else if (!middleName.equals(other.middleName))
            return false;
        if (nhisNumber == null) {
            if (other.nhisNumber != null)
                return false;
        } else if (!nhisNumber.equals(other.nhisNumber))
            return false;
        if (phoneNumber == null) {
            if (other.phoneNumber != null)
                return false;
        } else if (!phoneNumber.equals(other.phoneNumber))
            return false;
        if (placeOfBirth == null) {
            if (other.placeOfBirth != null)
                return false;
        } else if (!placeOfBirth.equals(other.placeOfBirth))
            return false;
        if (title != other.title)
            return false;
        if (town == null) {
            if (other.town != null)
                return false;
        } else if (!town.equals(other.town))
            return false;
        return true;
    }

    /**
     *
     * @param file
     * @throws IOException
     *  Log method to save patient to a file.
     */
    public void log(String file) throws IOException {
        // Creating a new file writer class.
        FileWriter fw = new FileWriter(new File(System.getProperty("user.dir"), "/Files/"+ file),true);
            Path path = Paths.get(new File(System.getProperty("user.dir"), "/Files/"+ file).toURI());
            String info ="Patient{" +
                    "title='" + title +'\'' +
                    ",firstName='" + firstName + '\'' +
                    ", middleName='" + middleName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", ID='" + ID +'\'' +
                    ", gender='" + gender +'\'' +
                    ", dateOfBirth='" + dateOfBirth + '\'' +
                    ", age='" + age +'\'' +
                    ", placeOfBirth='" + placeOfBirth + '\'' +
                    ", homeAddress='" + homeAddress + '\'' +
                    ", town='" + town + '\'' +
                    ", bloodGroup='" + bloodGroup +'\'' +
                    ", nhisNumber='" + nhisNumber + '\'' +
                    ", phoneNumber='" + phoneNumber + '\'' +
                    ", email='" + email + '\'' +
                    ", date='" + date +'\'' +
                    ", lastEdited='" + lastEdited +'\'' +'}';

                // Splitting the patient information for easy  writing.
                String infos[] = info.split("'");
                // Setting a delimiter.
                String delimiter = " | ";

                // Appending the patient info to a file.
                for(int i = 1; i<infos.length;i+=2) {
                    Files.write(path, (infos[i] + delimiter).getBytes(), StandardOpenOption.APPEND);
                    //System.out.println(infos[i]);
                }
                Files.write(path, "\n".getBytes(), StandardOpenOption.APPEND);
                fw.close();

    }

    /**
     *  Read all patient method to read all patient on the saved file.
     */
    public static void readAllPatientsOnFile(){
        String line = "";
        String splitLine = " | ";

        // Using buffered reader to read information from the file.
        try(BufferedReader br = new BufferedReader(new FileReader( System.getProperty("user.dir")+"/Files/patientFiles.txt"))){
            int patientNumber = 1;
            // Reading each line to get each patient.
            while ((line = br.readLine()) != null){
                String[] row = line.split(splitLine);
                System.out.println("patient "+ patientNumber+":");
                for(int i = 0; i <row.length; i++){
                    System.out.print(row[i]+" ");
                }
                System.out.println("\n");
                patientNumber++;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     *  Read patient method to read patient on specific line.
     * @param patientNum
     */
    public static  void readpatientOnFile(int patientNum){
        String line = "";
        String splitLine = " | ";

        //  Using buffered reader to read from the file.
        try(BufferedReader br = new BufferedReader(new FileReader( System.getProperty("user.dir")+"/Files/patientFiles.txt"))){

            // Reading each  until reaching the specific line.
            for(int i = 0; i < patientNum; ++i){ line = br.readLine();}
            String[] row = line.split(splitLine);
            System.out.println("patient :");
            for(int i = 0; i <row.length; i++){
                System.out.print(row[i] + " ");
            }

            
        }catch (IOException e){
            e.printStackTrace();
        }
    
    }

    /**
     *  Main method to facilitate  execution of  methods.
     * @param args
     * @throws IOException
     */
    public static void main(String [] args) throws IOException {

                Patient bob = new Patient();
                bob.setPatientInfo(bob);
                bob.printProfile();
                bob.log("patientFiles.txt");

                // Creating patients for  testing  methoids.
                Patient patient1 = new Patient(titleNames.Mr, "Kwame", null, "Kwaku", genderType.male, "11/11/2000",18, "kumasi", "Kumasi", "kumasi", bloodGroupType.O_negative,"4546464", "02477565456", "kwame@gmail.com");
                Patient patient2 = new Patient(titleNames.Mr, "Kwabena", null, "Yaw", genderType.male, "11/12/1999",20, "Accra", "Accra", "Accra", bloodGroupType.O_positive,"48977964", "056545456", "kwabena@gmail.com");
                Patient patient3 = new Patient(titleNames.Mrs, "Ama", "Tawiah", "Antwi", genderType.female, "20/11/2000",19, "kwabenya", "kwabenya", "kwabenya", bloodGroupType.AB_negative,"3218797", "02477325456", "amaantwi@gmail.com");
                Patient patient4 = new Patient(titleNames.Miss, "Efua", null, "kantanka", genderType.female, "30/06/2001",18, "Tafo", "Tafo", "Tafo", bloodGroupType.B_negative,"988964", "02470000456", "efua@gmail.com");
                Patient patient5 = new Patient(titleNames.Mr, "Kwaku", null, "Osei-Tutu", genderType.male, "24/08/1999",20, "kumasi", "Kumasi", "kumasi", bloodGroupType.A_negative,"3231336", "024697667", "bobbie@gmail.com");
                Patient patient6 = new Patient(titleNames.Mrs, "leticia", null,  "Ntim", genderType.female, "31/1/2001",17, "Mampong", "Mampong", "Mampong", bloodGroupType.AB_positive,"null", "null", "null");
                Patient patient7 = new Patient(titleNames.Mrs, "Francisca", null, "kankam", genderType.female, "30/11/2003",15, "Aburi", "Aburi", "Aburi", bloodGroupType.O_positive,"null", "null", "null");
                Patient patient8 = new Patient(titleNames.Mr, "Rockson", "Ntiamoah", "Boateng", genderType.male, "12/12/2005",13, "Santaase", "Santaase", "Santaase", bloodGroupType.B_positive,"649549", "0265431332", "rockson@gmail.com");
                Patient patient9= new Patient(titleNames.Mr, "Majid", "Antwi", "Osman", genderType.male, "13/10/2000",18, "Ejisu", "Ejisu", "Ejisu", bloodGroupType.A_positive,"null", "null", "null");
                Patient patient10= new Patient(titleNames.Mrs, "Erica", null, "Betsy", genderType.female, "25/12/2016",2, "Akaase", "Akaase", "Akaase", bloodGroupType.B_positive,"87987994", "05769766325", "erica@gmail.com");
                Patient [] patientTest = { patient1, patient2,patient3, patient4, patient5,patient6, patient7, patient8, patient9, patient10};

                 HashMap test = new HashMap();
//                 a.add(patient1);

        test.add(bob);
        test.get(bob.getID()).printProfile()

    }

}
