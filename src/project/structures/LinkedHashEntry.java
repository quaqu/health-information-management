// Package name
package project.structures;

/**
 * Definitions of a linked list class for hash map.
 */
public class LinkedHashEntry {

    // Instantiating the  variable for the for class
    private int key;

    private Patient value;

    private LinkedHashEntry next;


    /**
     *  Overloaded constructor for the linked list.
     * @param key
     * @param value
     */
    LinkedHashEntry(int key, Patient value) {

          this.key = key;

          this.value = value;

          this.next = null;

    }


    /**
     *  Accessor method  for the value
     * @return
     */
    public Patient getValue() {

          return value;

    }


// Mutator method for the value
    public void setValue(Patient value) {
        if (value != null){
          this.value = value;
        }

    }


// Accessor method for the key
    public int getKey() {

          return key;

    }


// Accessor method for the next patient
    public LinkedHashEntry getNext() {

          return next;

    }


// Mutator method for the next patient
    public void setNext(LinkedHashEntry next) {

          this.next = next;

    }

}