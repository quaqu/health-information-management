// Package name;
package project.structures;

/**
 *  Definition of the Hashmap class
 */
public class HashMap {

    // Instant variables.
    private  static int TABLE_SIZE = 11;
    int size;

    LinkedHashEntry[] table;


// Hash map constructor
  public   HashMap() {

          table = new LinkedHashEntry[TABLE_SIZE];

          for (int i = 0; i < TABLE_SIZE; i++)

                table[i] = null;

    }

    /**
     * Get method to retrieve object from the hash table
     * @param key
     * @return
     */
    public  Patient get(int key) {
            Patient temp = null;
          int hash = (key % TABLE_SIZE);

          if (table[hash] == null)

                System .out.println("No patient exist for the key");

          else {

                LinkedHashEntry entry = table[hash];

                while (entry != null && entry.getKey() != key)

                      entry = entry.getNext();

                if (entry == null)

                      System.out.println("No patient exist for key");

                else

                      temp = entry.getValue();

          }
            return temp;
    }

    /**
     *  Add method to append patient to the hash table
     * @param patient
     */
    public void add(Patient patient){
        add(patient.getID(), patient);
}

    /**
     *  Add method for appending patient to hash table.
     * @param key
     * @param value
     */
    public void add (int key, Patient value) {

          int hash = (key % TABLE_SIZE);

          if (table[hash] == null)

                table[hash] = new LinkedHashEntry(key, value);

          else {

                LinkedHashEntry entry = table[hash];

                while (entry.getNext() != null && entry.getKey() != key )

                      entry = entry.getNext();

                if (entry.getKey() == key)

                      entry.setValue(value);

                else

                      entry.setNext(new LinkedHashEntry(key, value));

          }
          size ++;

    }

    /**
     *
     * @param patient
     */
    public void remove(Patient patient){
        remove(patient.getID());
}

    /**
     * Remove function for delete patient from hash table
     * @param key
     */
    public void remove(int key) {

          int hash = (key % TABLE_SIZE);

          if (table[hash] != null) {

                LinkedHashEntry prevEntry = null;

                LinkedHashEntry entry = table[hash];

                while (entry.getNext() != null && entry.getKey() != key) {

                      prevEntry = entry;

                      entry = entry.getNext();

                }

                if (entry.getKey() == key) {

                      if (prevEntry == null)

                           table[hash] = entry.getNext();

                      else

                           prevEntry.setNext(entry.getNext());

                }

          }
        size --;
    }

}